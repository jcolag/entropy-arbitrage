---
layout: post
title: 🔭 Looking Back on 2022
date: 2021-12-25 08:05:15-0500
categories:
tags: [retrospective, newyear]
summary: Interesting discoveries from 2022
thumbnail: /blog/assets/P20210331AS-1892-51131137370.png
offset: -30%
---

![Joe Biden Pitching "Build Back Better"](/blog/assets/P20210331AS-1892-51131137370.png "Regardless of whether it goes forward, I'd argue that Build Back Better has been the defining story of the United States in 2021...")

As I've tried to do on the last Sunday of every year that I've run **Entropy Arbitrage**, I wanted to take at least *one* post to look back on what 2022 looked like from my weird space, and maybe pat myself on the back over projects that I managed to pull together and release since the beginning of the year.  I mostly base the format on the [previous end-of-year posts](/blog/tag/newyear), though it does change every year.

For those of you who read the [**Entropy Arbitrage** newsletter](https://buymeacoffee/jcolag), the format should look similar, with thoughts that don't feel sufficiently fleshed out to warrant their own post, but too complex or wordy to be a social media post.

## Culture

This year, I noticed a few interesting things about how people consume and process politics and popular culture.

### Stand Your Ground...When It Benefits Me

I've written a bit, here and there, about the [recent Twitter takeover]({% post_url 2022-05-01-twitter %}).  I've talked about the [elephant in the room of alternatives]({% post_url 2022-11-20-mastodon %}), and satirically questioned whether [Twitter's implosion joins a larger trend]({% post_url 2022-11-13-social %}).  In a few cases, I've posted links to stories pointing to a broader agenda to destroy spaces where people freely criticize billionaires, or as a way to disrupt activism in general by sowing distrust.

However, I haven't talked about the reaction, which both amuses and worries me.

I mean, specifically, that we can see a contingent of Twitter "power-users" out there, insisting that we should refuse to cede this privately owned space to the right-wing people who...own it.  Please, they beg, do not abandon Twitter, because if you do, the bad guys will surely win.

Especially when these users have podcasts, though, I can't help but feel an irony that they loudly abandoned Spotify as a distribution platform for buying exclusive rights to a popular (though boring and right-wing) podcast, because they didn't want to associate with and support that business model.  However, they consider it a moral imperative to stay on Twitter.

It sounds odd, until they talk about why they know that many people have already ditched Twitter:  They see their follower counts dropping.  In other words, they consider Twitter more important than Spotify, because they don't know how to rebuild their audience on another platform, whereas they can ask listeners to change podcast providers.

And I don't mean to thoroughly demean the feelings behind this attitude.  I realize that, when your income relies primarily on viral tweets bringing listeners and readers to your work, you probably don't appreciate the idea of platforms that don't prioritize and encourage virality.  I also realize that the majority of people leaving now mostly identify as white, and so just now feel the abuse that others have endured on Twitter since it launched.  But still, I wish that they'd honestly say that they worry about their metrics more than they do the principle of protecting spaces...

## A Virus in the Writers Room?



## Technology

Most of my day, as you can probably guess, involves some aspect of a computer.

### Developing Software



## Hardware

I should probably mention that the "spare laptop" that I started using and complained about some in [my 2021 review]({% post_url 2021-12-26-review-2021 %}) works significantly better, after some careful adjustments.  I opened the case and rebuilt the power switch and other switches, fixing that problem completely, for the moment.  I also offloaded a huge amount of data (IMAP e-mail) that I hadn't used enough to warrant keeping it close.

The latter change sped up the time to pubilsh blog entries dramatically, as a side effect, so those reading through RSS may have noticed that the official publication time has gone back down to within two to four minutes of the actual release.

I also finally picked up one of a class of gadget that I've wanted for a long time:  A drawing tablet and display.  I've promised myself that I'd learn to draw for many years, prices have finally dropped to the point where it feels like a reasonable purchase, and the quality has gotten good enough that I can justify it as a second monitor for the laptop, with better resolution than the laptop screen.

For those looking for something similar, I went with the [Wacom One Creative Pen Tablet](https://www.wacom.com/en-us/products/pen-displays/wacom-one).  Ubuntu didn't need any software to get it working, but I did need to buy a DisplayPort-to-HDMI adapter, because of this weird laptop.  It does crowd my tiny workspace a bit, though, meaning that I probably won't get used to working with it full-time.

At some point, I'll start posting my progress on drawing, or at least post the occasional doodle.

## Releases

I mess around with a lot of projects, but rarely bother to look back on what I've accomplished.  So, with the new year ahead, it's time for me to do that...even though I'm disappointed that my "big" semi-secret project *still* isn't finished.

### Daily Iungimoji

To go with the [Daily Nonogram]({% post_url 2021-02-21-nonogram %}), I launched a memory/pair-matching game that I call the [Daily Iungimjoi]({% post_url 2022-03-06-iungimoji %}).  Later in the year, I rewrote parts of it to host on GitHub, instead of my personal server.

### Daily G.L.O.B.E.

Much like the Daily Iunjimoji above, I also launched a daily geography search that I call the [Daily G.L.O.B.E.]({% post_url 2022-03-13-globe %}), or "Geo-Locate Objects Before Enemies."  Later in the year, I expanded some story aspects of the game, and rewrote parts to host it on GitHub.

### Chasing Phantoms

While I didn't make a formal announcement, in some ways, I re-released [Chasing Phantoms](https://jcolag.github.io/ChasingPhantoms/), again rewritiing parts to host it on GitHub.

### Sunday Rants

This year, I've written about a nice range of topics.

 * Self-improvement, such as [sleeping]({% post_url 2021-01-10-sleep %}), [opting out of public records searches]({% post_url 2021-08-01-records %}), and [learning stenography]({% post_url 2021-11-28-steno %}),
 * Social justice, including [race and reparations]({% post_url 2021-02-14-reparations %}), [gender]({% post_url 2021-02-28-genders %}), the [primary social movements driving society]({% post_url 2021-05-30-winning %}), [being an ally]({% post_url 2021-06-06-do-work %}), [Juneteenth]({% post_url 2021-06-20-juneteenth %}), [content advisories]({% post_url 2021-07-04-advisory %}), counter-productive nature of [generational theory]({% post_url 2021-08-15-generations %}), and [abortion]({% post_url 2021-09-05-roe %}),
 * Media criticism, such as [racial dynamics in superhero fiction]({% post_url 2021-03-07-super %}), representation of [Asian women in fiction]({% post_url 2021-03-21-asian %}), the [lack of media websites that care]({% post_url 2021-05-02-wanted %}), the [Free Software Foundation's ethical mess]({% post_url 2021-05-09-fsf %}), [how media deals with moral issues]({% post_url 2021-07-11-mmedia %}), and the [comprehensive misunderstanding about how superheroes should work]({% post_url 2021-11-21-super %}),
 * Technical and career topics, such as [estimating schedules]({% post_url 2021-01-27-estimate %}), [CSS dark modes]({% post_url 2021-03-24-darkmode %}), [cryptocurrency]({% post_url 2021-05-16-crypto %}), [prioritization and scheduling]({% post_url 2021-06-13-priorities %}), [maybe-useful approaches to hiring]({% post_url 2021-06-27-hiring %}), the [use of a Stack Overflow account in education]({% post_url 2021-07-25-stack %}), the [value of GitHub Copilot]({% post_url 2021-07-18-copilot %}), and the [stagnation in web frameworks]({% post_url 2021-08-08-framework %}).

I also launched the irregular [Let's Fix...](/blog/tag/lets-fix) series of posts, though there's only one post, so far.

The posts aren't all as polished as I wish they were---several of them were intended to be published later but moved up the schedule due to align more sensibly with then-current events---but I think that I'll be able to continue standing behind what I wrote in them, and I can always go back for another round of editing.

### Fiction

While I wasn't able to pull a second novel together before the end of the year---it has been sitting mostly abandoned for at least six months, in fact---I did release an attempt at a [Halloween horror story]({% post_url 2021-10-31-gevkahahal %}), a science-fantasy take on [the War on Christmas]({% post_url 2021-12-05-war %}), and a transcription of [*Calafia, Queen of California*]({% post_url 2021-03-28-calafia %}), itself an adaptation of a medieval Spanish story.

### Entropy Arbitrage

I made significant---though mostly invisible---changes to the blog, over the course of the year, from adding plugins to make the source code more sensible, to investigating different emoji fonts, to identifying posts that have changed and how much they've changed.

That's in addition to concluding the blog's second year, with the five hundredth post published.

I have also kept up [the mailing list](https://entropy-arbitrage.mailchimpsites.com/), with updates on a couple of the posts mentioned above, in addition to improving that code.  Maybe the biggest initiative was finally getting the blog sign-up (to the right of the page, if you're reading this on the blog) to work regardless of ad-blockers.

## Personal

Finally, I've learned a thing or two about myself, I think.

### Cord-Cutting After-Effects

In April, I [dropped cable service]({% post_url 2021-04-04-cord-cut %}), and I have consistently felt great about that decision.  There's more money in my budget, with less money going into the pockets of monopolies, and I'm not maintaining a service that I don't really use.  Locast has unfortunately fallen apart, depriving me of some of my favorite casual viewing, but I have found enough substitutions that it doesn't *really* matter.  Oh, and speaking of cord-cutting activities, I have come to the conclusion that BillFixers can't be trusted to perform more than the most straightforward tasks---and maybe even to just act professionally---so I can no longer recommend using them.  I added an update explaining in the cord-cutting post, last week, so I won't waste time on it here.

Likewise, canceling Netflix---until this month, actually, to catch up on shows, before canceling again---has brought me nothing but joy, as they send the most *inept* customer retention e-mails that I have ever seen.  Now that I'm back, I'm reminded of one huge reason why I dropped Netflix:  They don't support their own shows, expecting people to discover (for example) **On My Block**, rather than pushing it into recommendations or advertising it outside the service; I was responsible for learning that it exists, though they'll constantly recommend shows that I've *already* watched.

Amazon Prime is now in my sights, too, as we approach the end of my annual subscription; I'll probably come back for a month if the second season of **Undone** ever shows up.  As an aside, I should probably mention that I *originally* signed up for Amazon Prime (with a decent discount) specifically to watch their original shows---it may have been a promotion for **Transparent**---with the delivery just a minor convenience.  Then, they got me hooked on the delivery for a couple of years, which definitely isn't healthy.  Even that convenience is now mostly useless, though, as I've found better sources to order various goods.  So, if I'm watching less Prime Video, it makes no sense to keep paying Amazon, since there isn't much evidence that the company is going to spend it on improving the world in any way.  More likely, they use it to research better ways to [trap employees in the paths of tornadoes](https://www.voanews.com/a/dozens-killed-in-unseasonal-us-tornadoes-with-a-long-path-of-destruction/6351046.html) with [no protection](https://otherwords.org/tornadoes-can-kill-so-can-amazons-business-model/).

By contrast, my [Emby](https://emby.media/) server---which I still often consider replacing with [Jellyfin](https://jellyfin.org/) to reduce my dependence on proprietary code---has remained a joy.  One local computer hosts a streaming service that includes my CD collection, my DVD collection, legal downloads from various independent sources, some recordings from a primitive, improvised DVR that I rigged up many years ago (a show that I've wanted to rewatch, never got a home video release, and isn't streaming anywhere, no less), and so forth.  If it had a book reader instead of just a link to download books to a browser, it'd be close to ideal.

Quick aside:  Actually ideal would probably include an online *comic* reader that can pan and zoom from panel to panel, like most of the corporate comic readers have.  But that's obviously difficult *and* requires metadata on each comic page to explain where the panels are and what order to show them.  There are desktop reader applications that try to do that if the metadata exists, but I'd bet that none have been implemented for Roku, and the metadata is as rare as it is tedious to enter.

### New Skills

As I wrote about last month, I decided to [finally learn stenography]({% post_url 2021-11-28-steno %}), which seems like it'll be fast enough for me to use it routinely soon.

And while it's not a particularly "new" skill for me, I have also spent more time sewing than I have in a long time---using patterns from [Free Sewing](https://freesewing.org/)---to reduce my reliance on supply chains.  I'm not good at it, yet, not even as good as I was as a teenager taking a junior high school home economics class, but I'm definitely improving.

Depending on how things go, I may expand this in the new year.  As mentioned [a couple of weeks ago]({% post_url 2021-12-17-week %}), for example, I might try to pick up a new language.

### Social Media Changes

While I still support the ecosystems, of course, I've become functionally inactive on the [Free Software social networking platforms](/blog/tag/socialshowdown).  I check in on Scuttlebutt almost daily, but have only sent out a couple of posts since last winter; actually, that's a lie, because the "spare laptop" that I've been using has such a tiny hard drive that I haven't been able to *run* Scuttlebutt code without filling up the disk.  I log into Diaspora to manually post blog updates, since I haven't successfully automated that.  And I automatically post to Mastodon, and manually log in every couple of weeks when I realize that I shouldn't abandon my account to a bot.  My twtxt feed is almost entirely bot activity, and I barely check in there.

Even The Practical Dev---not covered as part of the "Showdown," but the underlying software Forem is open source and has features that try to build a community---has mostly collapsed into that special kind of clickbait that perpetuates dumb arguments about whose system is "better."  Spoiler alert:  Their "evidence" for something being better is invariably that they're accustomed to it, because it's similar to what they were taught in school.  Some things never change, I guess...

By contrast, I've been semi-active on Twitter, odd as it feels.  I've stopped replying to horrible politicians, though, since "engagement" just gives them a higher profile with the algorithm.  Best to just report them for encouraging self-harm, when they complain about vaccines or whatever.  Though it's extremely unfortunate, since I didn't make those comments just for the catharsis.  I make them partly because a reader less invested in their rhetoric either way might benefit from a contrary view grounded in fact, and partly because every post needling them diffuses the targets that they can aim at.

I'm still not on social media more than half an hour per day, though, and I still recommend keeping similar limits for all social networks.  The easiest way to prevent what's now called "doom-scrolling" is to not be in a position where it's an option...

The central problem seems to be one of the things that I mentioned in that social networking showdown:  Not even the people who claim to support Free Software spend much actual time on these networks, so what space isn't empty is instead filled by bots copying posts over from proprietary networks.  And that sense of the community or *lack* of a community is infectious:  If the only people on a network are the people talking about the network or are just blindly reposting from elsewhere, why bother?  There's a small exception with Scuttlebutt:  I like many of the people there, but the overwhelming majority of the actual conversations---ignoring the people using the network as a diary---are about the Scuttlebutt protocol itself and decentralized software in general.

Of course, there's a solution to all of this:  Get more people off the corporate-owned networks and into these communities...who I actually want to interact with.  So, I'm always prepared to return to those networks, should a bunch of readers start showing up there, for example.  There just needs to be something to talk about.  Of course, I also haven't abandoned them, so much as look at them less.

### Pacing Myself

Over the course of the year, I had plans to bring the [*Real Life in Star Trek*](/blog/tag/startrek) series to (nearly) a close, finish a novel, release at least one non-fiction book, launch three web services taking the business side seriously, learn animation, and get back into sewing, in addition to dealing my usual day-to-day life.  As you can probably guess from the earlier discussion, most of that didn't happen.  I overloaded myself, and accomplished less than if I had put off lower-priority projects until next year.

That is---and I'm being specific, here, because I want to illustrate the load problem on the chance it helps someone else catch an issue for themselves, not to dwell on failures---I never got around to building an audience for **All Around the News**, pushing to sell advertising, or adding native advertising features to the article list.  Instead of building it into a self-sustaining business, I created a "good enough" toy and moved onto the next project.  The novel fell apart, because the idea changed from a simple story with a central metaphor to a model for a season of television with heavy allegorical elements, and the setting didn't have the resilience to support the latter.  I have a few unfinished garments cut and half-sewn.  I added extra work to the *Star Trek* posts---**The Animated Series** novels, to be specific---and eventually realized that I just wasn't able to keep up.

That's just a sample, which doesn't even get to the projects that haven't seriously looked at.  For example, I briefly considered creating my own streaming service with cheap and free content, which didn't get past a note about the fact that I could probably do it.  My point is that I need to keep better watch over what I commit myself to doing, even just to myself.  I also may need to create regular blocks of time to handle certain kinds of projects, so that I can ensure seeing some steady progress.  My current "try to get something in before seven in the morning, and then see where the day takes me" approach is clearly not sufficient.

In fact, one of the reasons that I've been looking at the aforementioned new skills is to slow myself down a bit.  That is, if I'm at my sewing machine, I can't stare at a screen.

## Conclusions

In last year's end-of-year post, I said the following.

 > Growth takes affirmative effort and isn't something that just happens, whereas it's surprisingly easy to neglect yourself into bad habits.

That still applies, today.  I often laugh/complain that technology companies will constantly and rightly tell you that "you can't improve what you don't measure," but then turn around to refuse to measure anything of importance, because they "already know" how to improve.  It's not a lesson that we should take for our private lives.  Do the hard work.  It's worth the trouble.

Enough about me, though...well, at least until tomorrow's [developer journal](/blog/tag/devjournal) post and next Sunday's look ahead at 2022.  How was *your* 2021?

* * *

**Credits**: The header image is [President Joe Biden delivers remarks on his economic vision](https://www.flickr.com/photos/whitehouse/51131137370/) by Adam Schultz, in the public domain as a work of the United States government.
